package tes.ittron.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import tes.ittron.Adapter.OurTeamAdapter;
import tes.ittron.Adapter.OurmarketAdapter;
import tes.ittron.Adapter.OurmarketAdapter2;
import tes.ittron.Adapter.ProduksatuAdapter;
import tes.ittron.Objek.ObjOurMarket;
import tes.ittron.Objek.ObjRecommend;
import tes.ittron.R;

public class PageAbout extends Fragment {

    Kaget kaget;

    OurTeamAdapter ourTeamAdapter;
    RecyclerView ourteam;
    List<ObjRecommend> objRecommendList;

    OurmarketAdapter2 ourmarketAdapter2;
    RecyclerView rvourmarket;
    List<ObjOurMarket> objOurMarketList;

    public PageAbout() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.page_about, container, false);

        init(view);

        return view;
    }

    void init(View xxx){
        ourteam = xxx.findViewById(R.id.ourteam);
        objRecommendList = new ArrayList<>();
        dataRecommend();
        ourTeamAdapter = new OurTeamAdapter(getActivity(), objRecommendList);
        ourteam.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        ourteam.setAdapter(ourTeamAdapter);

        rvourmarket = xxx.findViewById(R.id.ourservice);
        objOurMarketList = new ArrayList<>();
        dataOurMarket();
        ourmarketAdapter2 = new OurmarketAdapter2(getActivity(), objOurMarketList);
        rvourmarket.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvourmarket.setAdapter(ourmarketAdapter2);
    }

    void dataOurMarket(){
        objOurMarketList.add(new ObjOurMarket(R.drawable.z1, "Branding", ""));
        objOurMarketList.add(new ObjOurMarket(R.drawable.z2, "Product Knowledge", ""));
        objOurMarketList.add(new ObjOurMarket(R.drawable.z3, "Market Share", ""));
    }

    void dataRecommend(){
        objRecommendList.add(new ObjRecommend("Mahathir", ""));
        objRecommendList.add(new ObjRecommend("Ittron", ""));
        objRecommendList.add(new ObjRecommend( "Sunardi", ""));
        objRecommendList.add(new ObjRecommend( "Sulaiman", ""));
    }

    public interface Kaget{
        public void KirimanData(String kagetan);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            kaget = (Kaget) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        kaget = null;
        super.onDetach();
    }
}