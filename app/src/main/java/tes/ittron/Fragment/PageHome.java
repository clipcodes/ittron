package tes.ittron.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import tes.ittron.Adapter.OurmarketAdapter;
import tes.ittron.Adapter.ProduksatuAdapter;
import tes.ittron.Objek.ObjOurMarket;
import tes.ittron.Objek.ObjRecommend;
import tes.ittron.R;

public class PageHome extends Fragment {

    Kaget kaget;
    OurmarketAdapter ourmarketAdapter;
    RecyclerView rvourmarket;
    List<ObjOurMarket> objOurMarketList;

    ProduksatuAdapter produk1Adapter;
    RecyclerView rvrecommend;
    List<ObjRecommend> objRecommendList;

    public PageHome() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.page_home, container, false);

        init(view);

        return view;
    }

    void init(View xxx){
        rvourmarket = xxx.findViewById(R.id.rvmarket);
        objOurMarketList = new ArrayList<>();
        dataOurMarket();
        ourmarketAdapter = new OurmarketAdapter(getActivity(), objOurMarketList);
        rvourmarket.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvourmarket.setAdapter(ourmarketAdapter);

        rvrecommend = xxx.findViewById(R.id.rvrecommend);
        objRecommendList = new ArrayList<>();
        dataRecommend();
        produk1Adapter = new ProduksatuAdapter(getActivity(), objRecommendList);
        rvrecommend.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        rvrecommend.setAdapter(produk1Adapter);
    }

    void dataOurMarket(){
        objOurMarketList.add(new ObjOurMarket(R.drawable.img1, "Branding", ""));
        objOurMarketList.add(new ObjOurMarket(R.drawable.img4, "Product Knowledge", ""));
        objOurMarketList.add(new ObjOurMarket(R.drawable.img2, "Market Share", ""));
        objOurMarketList.add(new ObjOurMarket(R.drawable.img3, "Atfordable Price", ""));
    }

    void dataRecommend(){
        objRecommendList.add(new ObjRecommend("Rumah Bagus Jos", ""));
        objRecommendList.add(new ObjRecommend("Contoh Produk Ittron", ""));
        objRecommendList.add(new ObjRecommend( "Ittron Testing", ""));
        objRecommendList.add(new ObjRecommend( "Coba Coba Saja", ""));
    }

    public interface Kaget{
        public void KirimanData(String kagetan);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            kaget = (Kaget) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        kaget = null;
        super.onDetach();
    }
}