package tes.ittron.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import tes.ittron.Adapter.OurmarketAdapter;
import tes.ittron.Adapter.ProdukduaAdapter;
import tes.ittron.Adapter.ProduksatuAdapter;
import tes.ittron.Objek.ObjRecommend;
import tes.ittron.Objek.ObjectRumah;
import tes.ittron.R;
import tes.ittron.Utils.AllReqs;
import tes.ittron.Utils.Utils;

public class PageProduct extends Fragment {

    String TAG = getClass().getSimpleName();
    Kaget kaget;

    ProdukduaAdapter produkduaAdapter;
    RecyclerView rvproduct;
    List<ObjectRumah> objRecommendList;
    RequestQueue requestQueue;

    public PageProduct() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.page_product, container, false);

        Utils.MahathirOptionGambar(getActivity());

        init(view);

        new AllReqs(getActivity(), requestQueue).jsonobjectget(response);
        return view;
    }

    void init(View xxx){
        requestQueue = Volley.newRequestQueue(getActivity());
        rvproduct = xxx.findViewById(R.id.rvproduct);
        objRecommendList = new ArrayList<>();
        produkduaAdapter = new ProdukduaAdapter(getActivity(), objRecommendList);
        rvproduct.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvproduct.setAdapter(produkduaAdapter);
    }

    Response.Listener<JSONObject> response = new Response.Listener<JSONObject>() {
        @Override
        public void onResponse(JSONObject response) {
            Log.d("RESPON DATA", response.toString());
            try {
                JSONArray jsonArray = response.optJSONArray("data");
                for (int i=0;i<jsonArray.length();i++){
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    ObjectRumah objectRumah = new ObjectRumah();
                    objectRumah.setNamarumah(jsonObject.getString("namarumah"));
                    objectRumah.setTextsingkat(jsonObject.getString("textsingkat"));
                    objectRumah.setFoto(jsonObject.getString("foto"));
                    objectRumah.setHarga(jsonObject.getString("harga"));
                    objectRumah.setLuas(jsonObject.getString("luas"));
                    objectRumah.setRuangtidur(jsonObject.getString("ruangtidur"));
                    objectRumah.setKolamrenang(jsonObject.getString("kolamrenang"));
                    objectRumah.setRuangtamu(jsonObject.getString("ruangtamu"));
                    objectRumah.setKategori(jsonObject.getString("kategori"));

                    objRecommendList.add(objectRumah);
                }

                produkduaAdapter.notifyDataSetChanged();
            } catch (JSONException e){
                Log.w(TAG, "Err: "+e.getMessage());
            }
        }
    };

//    void dataRecommend(){
//        objRecommendList.add(new ObjRecommend("Rumah Bagus Jos", ""));
//        objRecommendList.add(new ObjRecommend("Contoh Produk Ittron", ""));
//        objRecommendList.add(new ObjRecommend( "Ittron Testing", ""));
//        objRecommendList.add(new ObjRecommend( "Coba Coba Saja", ""));
//    }

    public interface Kaget{
        public void KirimanData(String kagetan);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            kaget = (Kaget) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        kaget = null;
        super.onDetach();
    }
}