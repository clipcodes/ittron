package tes.ittron.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import tes.ittron.R;

public class PageContact extends Fragment {

    Kaget kaget;

    public PageContact() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.page_contact, container, false);


        return view;
    }

    public interface Kaget{
        public void KirimanData(String kagetan);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            kaget = (Kaget) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        kaget = null;
        super.onDetach();
    }
}