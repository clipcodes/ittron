package tes.ittron.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import tes.ittron.MainActivity;
import tes.ittron.Objek.ObjectPengguna;
import tes.ittron.R;
import tes.ittron.Utils.AllReqs;
import tes.ittron.Utils.Pref;

public class SignIn extends Fragment {

    Kaget kaget;
    CardView go;
    ProgressBar loding;
    RequestQueue requestQueue;
    EditText email, pass;
    TextView err;
    TextView logintex;

    public SignIn() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.signin, container, false);

        init(view);

        view.findViewById(R.id.gosignup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kaget.KirimanData("Sign Up");
            }
        });
        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String e = email.getText().toString();
                String p = pass.getText().toString();
                err.setText("");

                if (e.isEmpty()){
                    err.setText("Please Fill Email");
                    return;
                }

                if (p.isEmpty()){
                    err.setText("Please Fill Password");
                    return;
                }

                loding.setVisibility(View.VISIBLE);
                logintex.setVisibility(View.GONE);
                go.setCardBackgroundColor(getActivity().getResources().getColor(R.color.putih));
                new AllReqs(getActivity(), requestQueue).requesjos(res, e, p);
            }
        });

        return view;
    }

    void init(View view){
        err = view.findViewById(R.id.err);
        logintex = view.findViewById(R.id.logintex);
        requestQueue = Volley.newRequestQueue(getActivity());
        email = view.findViewById(R.id.email);
        pass = view.findViewById(R.id.pass);
        go = view.findViewById(R.id.go);
        loding = view.findViewById(R.id.loding);
    }

    private Response.Listener<String> res = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            Log.i("RESPON", response);
            try {
                JSONObject object = new JSONObject(response);
                if (response.contains("pesan")){
                    err.setText(object.getString("pesan"));
                    loding.setVisibility(View.GONE);
                    logintex.setVisibility(View.VISIBLE);
                    go.setCardBackgroundColor(getActivity().getResources().getColor(R.color.textcolor));
                } else {
                    Pref.setLogin(getActivity(), true);
                    Intent i = new Intent(getActivity(), MainActivity.class);
                    startActivity(i);
                    getActivity().finish();
                }
            } catch (JSONException e) {
                Log.i("RESPON EROR", e.getMessage());
            }
        }
    };

    public interface Kaget{
        public void KirimanData(String kagetan);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            kaget = (Kaget) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement TextClicked");
        }
    }

    @Override
    public void onDetach() {
        kaget = null;
        super.onDetach();
    }
}