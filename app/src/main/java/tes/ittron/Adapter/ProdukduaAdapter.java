package tes.ittron.Adapter;

import android.app.Activity;
import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import tes.ittron.Objek.ObjectRumah;
import tes.ittron.R;

import static tes.ittron.Utils.Konsta.DOMAIN;

public class ProdukduaAdapter extends RecyclerView.Adapter<ProdukduaAdapter.ViewHolder>{

    Activity activity;
    List<ObjectRumah> items;
    OnClick onItemClickListener;

    public ProdukduaAdapter(Activity activity, List<ObjectRumah> items){
        this.activity = activity;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = activity.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.modelitem_produkdua, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.judul.setText(items.get(position).getNamarumah());
        holder.harga.setText("$"+items.get(position).getHarga());
        holder.kategori.setText(items.get(position).getKategori());
        holder.luas.setText(items.get(position).getLuas());
        holder.kamar.setText(items.get(position).getRuangtidur());
        holder.ruang.setText(items.get(position).getRuangtamu());
        holder.kolam.setText(items.get(position).getKolamrenang());
        ImageLoader.getInstance().displayImage(DOMAIN+ "foto/" +items.get(position).getFoto(), holder.foto);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        TextView judul, harga, kategori, luas, kamar, ruang, kolam;
        ImageView foto;

        public ViewHolder(final View itemView) {
            super(itemView);
            foto = itemView.findViewById(R.id.foto);
            judul = itemView.findViewById(R.id.judul);
            harga = itemView.findViewById(R.id.harga);
            kategori = itemView.findViewById(R.id.kategori);
            luas = itemView.findViewById(R.id.luas);
            kamar = itemView.findViewById(R.id.kamar);
            ruang = itemView.findViewById(R.id.ruang);
            kolam = itemView.findViewById(R.id.kolam);
        }
    }

    public void setOnItemClickListener(OnClick clickListener) {
        onItemClickListener = clickListener;
    }
    public interface OnClick {
        void onItemClick();
    }
}
