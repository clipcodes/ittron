package tes.ittron.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import tes.ittron.Objek.ObjOurMarket;
import tes.ittron.R;

public class OurmarketAdapter extends RecyclerView.Adapter<OurmarketAdapter.ViewHolder>{

    Activity activity;
    List<ObjOurMarket> items;
    OnClick onItemClickListener;

    public OurmarketAdapter(Activity activity, List<ObjOurMarket> items){
        this.activity = activity;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = activity.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.modelitem_ourmarket, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.judul.setText(items.get(position).getJudul());
        holder.ikon.setImageDrawable(activity.getResources().getDrawable(items.get(position).getIkon()));

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        ImageView ikon;
        TextView judul;

        public ViewHolder(final View itemView) {
            super(itemView);
            ikon = itemView.findViewById(R.id.ikon);
            judul = itemView.findViewById(R.id.judul);
        }
    }

    public void setOnItemClickListener(OnClick clickListener) {
        onItemClickListener = clickListener;
    }
    public interface OnClick {
        void onItemClick();
    }
}
