package tes.ittron.Adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import tes.ittron.Objek.ObjRecommend;
import tes.ittron.R;

public class ProduksatuAdapter extends RecyclerView.Adapter<ProduksatuAdapter.ViewHolder>{

    Activity activity;
    List<ObjRecommend> items;
    OnClick onItemClickListener;

    public ProduksatuAdapter(Activity activity, List<ObjRecommend> items){
        this.activity = activity;
        this.items = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = activity.getLayoutInflater();
        View view = layoutInflater.inflate(R.layout.modelitem_produksatu, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.judul.setText(items.get(position).getJudul());

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder {

        TextView judul;

        public ViewHolder(final View itemView) {
            super(itemView);
            judul = itemView.findViewById(R.id.judul);
        }
    }

    public void setOnItemClickListener(OnClick clickListener) {
        onItemClickListener = clickListener;
    }
    public interface OnClick {
        void onItemClick();
    }
}
