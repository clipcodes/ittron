package tes.ittron;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import tes.ittron.Fragment.SignIn;
import tes.ittron.Fragment.SignUp;
import tes.ittron.Utils.Pref;

public class Login extends AppCompatActivity implements SignIn.Kaget, SignUp.Kaget {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.auth);

        if (Pref.isLogin(getApplicationContext())){
            Intent i = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(i);
            finish();
            return;
        }

        ngloadview(new SignIn());
    }

    @Override
    public void KirimanData(String kagetan) {
        switch (kagetan){
            case "Sign In":
                ngloadview(new SignIn());
                break;
            case "Sign Up":
                ngloadview(new SignUp());
                break;
        }
    }

    private boolean ngloadview(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
            return true;
        }
        return false;
    }
}
