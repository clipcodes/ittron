package tes.ittron;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import tes.ittron.Fragment.PageAbout;
import tes.ittron.Fragment.PageContact;
import tes.ittron.Fragment.PageHome;
import tes.ittron.Fragment.PageProduct;

public class MainActivity extends AppCompatActivity implements PageHome.Kaget, PageAbout.Kaget, PageContact.Kaget, PageProduct.Kaget {

    BottomNavigationView bottomNavigationView;


    @Override
    public void KirimanData(String kagetan) {

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mains);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        init();
        declare();
    }

    public void init(){
        bottomNavigationView = findViewById(R.id.bottomNavigationView);
    }

    void declare(){
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        bottomNavigationView.setSelectedItemId(R.id.home);
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(MenuItem item) {
            switch (item.getItemId()) {
                case R.id.home:
                    loadHalaman(new PageHome());
                    return true;
                case R.id.product:
                    loadHalaman(new PageProduct());
                    return true;
                case R.id.about:
                    loadHalaman(new PageAbout());
                    return true;
                case R.id.contact:
                    loadHalaman(new PageContact());
                    return true;
            }
            return false;
        }

    };

    private boolean loadHalaman(Fragment fragment) {
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.container, fragment)
                    .commit();
            return true;
        }
        return false;
    }
}
