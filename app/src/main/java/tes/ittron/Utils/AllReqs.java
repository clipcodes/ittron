package tes.ittron.Utils;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import tes.ittron.Objek.ObjectPengguna;

import static tes.ittron.Utils.Konsta.DAFTARBOS;
import static tes.ittron.Utils.Konsta.DOMAIN;
import static tes.ittron.Utils.Konsta.LOGINBOS;
import static tes.ittron.Utils.Konsta.RUMAHBOS;

public class AllReqs {

    Activity activity;
    RequestQueue requestQueue;

    public AllReqs(Activity activity, RequestQueue requestQueue) {
        this.activity = activity;
        this.requestQueue = requestQueue;
    }

//    Response.Listener<JSONArray> response = new Response.Listener<JSONArray>() {
//        @Override
//        public void onResponse(JSONArray response) {
//            Log.d("RESPON DATA", response.toString());
//            try {
//                for (int i=0;i<response.length();i++){
//                    JSONObject jsonObject = response.getJSONObject(i);
//                }
//            } catch (JSONException e){
//
//            }
//        }
//    };

//    Response.Listener<JSONObject> response = new Response.Listener<JSONObject>() {
//        @Override
//        public void onResponse(JSONObject response) {
//            Log.d("RESPON DATA", response.toString());
//            try {
//                JSONArray jsonArray = new JSONArray(response);
//                for (int i=0;i<jsonArray.length();i++){
//                    JSONObject jsonObject = jsonArray.getJSONObject(i);
//                }
//            } catch (JSONException e){
//
//            }
//        }
//    };


    public void requesreqjos(Response.Listener<String> loginsukses, final ObjectPengguna pengguna, final String pass){
        StringRequest postRequest = new StringRequest(Request.Method.POST, DOMAIN+DAFTARBOS, loginsukses,
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Log.i("RESPON EROR", "TimeoutError NoConnectionError");
                            Toast.makeText(activity, "Koneksi Error", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Log.i("RESPON EROR", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.i("RESPON EROR", "NetworkError");
                            Toast.makeText(activity, "Koneksi Tidak Ada", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Log.i("RESPON EROR", "ParseError");
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("email", pengguna.getEmail());
                params.put("namalengkap", pengguna.getYourname());
                params.put("phone", pengguna.getPhone());
                params.put("password", pass);
                return params;
            }
        };

        postRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(postRequest);
    }

    public void requesjos(Response.Listener<String> loginsukses, final String usr, final String pass){
        StringRequest postRequest = new StringRequest(Request.Method.POST, DOMAIN+LOGINBOS, loginsukses,
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Log.i("RESPON EROR", "TimeoutError NoConnectionError");
                            Toast.makeText(activity, "Koneksi Error", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Log.i("RESPON EROR", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.i("RESPON EROR", "NetworkError");
                            Toast.makeText(activity, "Koneksi Tidak Ada", Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Log.i("RESPON EROR", "ParseError");
                        }
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<>();
                params.put("email", usr);
                params.put("password", pass);
                return params;
            }
        };

        postRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(postRequest);
    }

    public void jsonarrayget(Response.Listener<JSONArray> responsukses, String url){
        Log.i(getClass().getSimpleName(), url);
        JsonArrayRequest getRequest = new JsonArrayRequest(Request.Method.GET, url, null,
                responsukses,
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError) {
                            Log.i("RESPON EROR", "TimeoutError NoConnectionError");
                        } else if (error instanceof NoConnectionError) {
                            Log.i("RESPON EROR", "NoConnectionError");
                        } else if (error instanceof ServerError) {
                            Log.i("RESPON EROR", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.i("RESPON EROR", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.i("RESPON EROR", "ParseError");
                        }
                    }
                }
        );

        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(getRequest);
    }

    public void jsonobjectget(Response.Listener<JSONObject> responsuks){
        JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, DOMAIN+RUMAHBOS, null,
                responsuks,
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                            Log.i("RESPON EROR", "TimeoutError NoConnectionError");
                        } else if (error instanceof ServerError) {
                            Log.i("RESPON EROR", "ServerError");
                        } else if (error instanceof NetworkError) {
                            Log.i("RESPON EROR", "NetworkError");
                        } else if (error instanceof ParseError) {
                            Log.i("RESPON EROR", "ParseError");
                        }
                    }
                }
        );

        getRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(getRequest);
    }

}
