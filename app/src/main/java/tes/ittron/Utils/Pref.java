package tes.ittron.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Pref {

    public static String baca(Context ctx, String namae, String isine) {
        SharedPreferences sharedPref = ctx.getSharedPreferences("ittron", Context.MODE_PRIVATE);
        return sharedPref.getString(namae, isine);
    }

    public static void tulis(Context ctx, String namae, String isine) {
        SharedPreferences sharedPref = ctx.getSharedPreferences("ittron", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(namae, isine);
        editor.apply();
    }

    public static boolean isLogin(Context c) {
        return Boolean.valueOf(Pref.baca(c, "login", "false"));
    }

    public static void setLogin(Context c, boolean login) {
        Pref.tulis(c, "login", String.valueOf(login));
    }
}
