package tes.ittron.Objek;

public class ObjRecommend {

    private String judul;
    private String desk;

    public ObjRecommend(String judul, String desk) {
        this.judul = judul;
        this.desk = desk;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDesk() {
        return desk;
    }

    public void setDesk(String desk) {
        this.desk = desk;
    }
}
