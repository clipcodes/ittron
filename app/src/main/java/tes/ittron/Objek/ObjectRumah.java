package tes.ittron.Objek;

public class ObjectRumah {

    private int id;
    private String namarumah;
    private String textsingkat;
    private String foto;
    private String harga;
    private String luas;
    private String ruangtidur;
    private String kolamrenang;
    private String ruangtamu;
    private String kategori;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamarumah() {
        return namarumah;
    }

    public void setNamarumah(String namarumah) {
        this.namarumah = namarumah;
    }

    public String getTextsingkat() {
        return textsingkat;
    }

    public void setTextsingkat(String textsingkat) {
        this.textsingkat = textsingkat;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getLuas() {
        return luas;
    }

    public void setLuas(String luas) {
        this.luas = luas;
    }

    public String getRuangtidur() {
        return ruangtidur;
    }

    public void setRuangtidur(String ruangtidur) {
        this.ruangtidur = ruangtidur;
    }

    public String getKolamrenang() {
        return kolamrenang;
    }

    public void setKolamrenang(String kolamrenang) {
        this.kolamrenang = kolamrenang;
    }

    public String getRuangtamu() {
        return ruangtamu;
    }

    public void setRuangtamu(String ruangtamu) {
        this.ruangtamu = ruangtamu;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }
}
