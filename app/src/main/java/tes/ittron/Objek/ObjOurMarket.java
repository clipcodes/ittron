package tes.ittron.Objek;

public class ObjOurMarket {

    private int ikon;

    private String judul;
    private String desk;

    public ObjOurMarket(int ikon, String judul, String desk) {
        this.ikon = ikon;
        this.judul = judul;
        this.desk = desk;
    }

    public int getIkon() {
        return ikon;
    }

    public void setIkon(int ikon) {
        this.ikon = ikon;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDesk() {
        return desk;
    }

    public void setDesk(String desk) {
        this.desk = desk;
    }
}
