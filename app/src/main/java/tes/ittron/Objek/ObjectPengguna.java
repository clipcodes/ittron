package tes.ittron.Objek;

import android.content.Context;

import tes.ittron.Utils.Pref;

public class ObjectPengguna {

    private String yourname;
    private String email;
    private String phone;

    public String getYourname() {
        return yourname;
    }

    public void setYourname(String yourname) {
        this.yourname = yourname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
